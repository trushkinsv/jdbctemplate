import com.jborned.vverh.service.search.StudentCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static liquibase.util.StringUtils.*;
import static liquibase.util.StringUtils.isNotEmpty;

@RequiredArgsConstructor
public class StudentRepositoryImpl implements StudentRepositoryCustom {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final StudentRowMapper rowMapper;

    @Override
    public List<String> getStringsByCriteria(StudentCriteria studentCriteria) {
        StringBuilder sqlQueryBuilder = new StringBuilder("select * from student s join center_admission_reason c on " +
                "s.center_admission_reason_id = c.id join birth_place b on s.birth_place_id = b.id where 1 = 1");
        Map<String, Date> sqlParams = new HashMap<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd");
        String[] stringCriteriaSplit;

        if (isNotEmpty(studentCriteria.getFirstName())) {
            stringCriteriaSplit = studentCriteria.getFirstName()
                    .trim()
                    .split(" +");
            for (String stringCriteria : stringCriteriaSplit) {
                sqlQueryBuilder.append(" and lower(s.first_name) like lower('%").append(stringCriteria).append("%')");
            }
        }

        if (isNotEmpty(studentCriteria.getMiddleName())) {
            stringCriteriaSplit = studentCriteria.getMiddleName()
                    .trim()
                    .split(" +");
            for (String stringCriteria : stringCriteriaSplit) {
                sqlQueryBuilder.append(" and lower(s.middle_name) like lower('%").append(stringCriteria).append("%')");
            }
        }

        if (isNotEmpty(studentCriteria.getLastName())) {
            stringCriteriaSplit = studentCriteria.getLastName()
                    .trim()
                    .split(" +");
            for (String stringCriteria : stringCriteriaSplit) {
                sqlQueryBuilder.append(" and lower(s.last_name) like lower('%").append(stringCriteria).append("%')");
            }
        }

        if (studentCriteria.getGender() != null) {
            sqlQueryBuilder.append(" and s.gender = '").append(studentCriteria.getGender()).append("'");
        }

        if (isNotEmpty(studentCriteria.getFromBirthDate())) {
            sqlQueryBuilder.append(" and s.birth_date >= :fromBirthDate");
            try {
                sqlParams.put("fromBirthDate", simpleDateFormat.parse(studentCriteria.getFromBirthDate()));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        if (isNotEmpty(studentCriteria.getToBirthDate())) {
            sqlQueryBuilder.append(" and s.birth_date <= :toBirthDate");
            try {
                sqlParams.put("toBirthDate", simpleDateFormat.parse(studentCriteria.getToBirthDate()));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        if (studentCriteria.getIsActive() != null) {
            sqlQueryBuilder.append(" and s.is_active = ").append(studentCriteria.getIsActive());
        }
        if(studentCriteria.getMonthOfBirth() != null){
            sqlQueryBuilder.append(" and MONTH(s.birth_date) = ").append(studentCriteria.getMonthOfBirth());
        }

        sqlQueryBuilder.append(" order by s.last_name, s.first_name, s.middle_name");

        return jdbcTemplate.query(sqlQueryBuilder.toString(), sqlParams, rowMapper);
    }
}
