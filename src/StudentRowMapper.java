import com.jborned.vverh.db.model.enumeration.FamilyStatus;
import com.jborned.vverh.db.model.enumeration.Gender;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

import static liquibase.util.StringUtils.*;
import static liquibase.util.StringUtils.isNotEmpty;

@Component
public class StudentRowMapper implements RowMapper<String> {

    private static final String DELIMITER = "\", \"";

    @Override
    public String mapRow(ResultSet resultSet, int i) throws SQLException {
        String reason = "";
        if (isNotEmpty(resultSet.getString("name"))) {
            reason = resultSet.getString("name");
        }

        String city = "";
        if (isNotEmpty(resultSet.getString("city"))) {
            city = resultSet.getString("city");
        }

        return "\"" + resultSet.getString("last_name") + " " +
                resultSet.getString("first_name") + " " +
                resultSet.getString("middle_name") + DELIMITER +
                Gender.valueOf(resultSet.getString("gender")).getText() + DELIMITER +
                resultSet.getString("birth_date").substring(0, 10) + DELIMITER +
                city + DELIMITER + resultSet.getString("email") + DELIMITER +
                resultSet.getString("contact_number") + DELIMITER +
                FamilyStatus.valueOf(resultSet.getString("status")).getText() + DELIMITER +
                resultSet.getString("relatives") + DELIMITER +
                resultSet.getString("passport_seria") + DELIMITER +
                resultSet.getString("passport_number") + DELIMITER +
                resultSet.getString("passport_issued_by") + DELIMITER +
                resultSet.getString("passport_issue_date").substring(0, 10) + DELIMITER +
                reason + DELIMITER + resultSet.getString("address_plan") + DELIMITER +
                resultSet.getString("address_fact") + DELIMITER +
                resultSet.getString("trusted") + "\"";
    }
}
